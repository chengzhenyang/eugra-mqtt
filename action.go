package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"
	"strconv"
	"time"

	MQTT "github.com/eclipse/paho.mqtt.golang"
)

//MQTT action Handler
var actions MQTT.MessageHandler = func(client MQTT.Client, msg MQTT.Message) {
	//Get current topic from MQTT topic by extracting numbers from the topic address e.g eugra/0/audio
	topic := msg.Topic()
	reg, err := regexp.Compile("[^0-9]+")
	if err != nil {
		log.Fatal(err)
	}
	str1 := reg.ReplaceAllString(topic, "")
	tableid, _ := strconv.Atoi(str1) //Set tableid from string to int

	//first unmarshals with turn to check turn.Action
	var turn Turn
	json.Unmarshal(msg.Payload(), &turn)
	if turn.Action == ("total_user") { //if action is total_user the receive does nothing
		return
	}
	log.Printf("actions() receive action topic:%s, payload:%v\n", topic, turn)
	if turn.Action == ("turn_start") || turn.Action == ("turn_end") { //if the action is turn_start or turn_end forwards to function to handle turns
		actionTurn(turn, tableid, client)
	}
	if turn.Action == ("nod") || turn.Action == ("shake") { //if the action is nod or shake forwards to function to handle gestures\
		var gesture Gesture
		json.Unmarshal(msg.Payload(), &gesture)
		if _, ok := gesturesmap[tableid]; !ok {
			gesturesmap[tableid] = []Gesture{}
		}
		gesturesmap[tableid] = append(gesturesmap[tableid], gesture)
		//actionGesture(gesture, tableid)
	}
	if turn.Action == ("join") || turn.Action == ("leave") { //if the action is join or leave forwards to function to handle user logs
		var member Member
		json.Unmarshal(msg.Payload(), &member)
		MemberChange(member, tableid, client)
	}

}

//MemberChange Function to log total users
func MemberChange(member Member, tableid int, client MQTT.Client) {
	if member.Action == "join" {
		//remove value from lastmembertime[tableid] as a member has joined
		if _, ok := lastmembertime[tableid]; ok {
			delete(lastmembertime, tableid)
		}
		//check if user is already inside the table so join is not duplicated
		i := -1
		for k, v := range totalmembers[tableid] {
			if member.User_id == v {
				i = k
			}
		}
		//if users is not in the array of totalmembers add user to array in the map
		if i < 0 {
			totalmembers[tableid] = append(totalmembers[tableid], member.User_id)
			log.Println("MemberChange() new user joined", totalmembers[tableid])
		}
		//Send MQTT message to actions to show info of the current table to new user
		var payload TotalMember
		payload.Action = "total_user"
		payload.Total_user = len(totalmembers[tableid])
		row := SQL.QueryRow("SELECT name FROM `eugra_users` WHERE id=?", current_speaker_id[tableid]) //Finds Name of the current speaker
		row.Scan(&payload.Speaker_name)
		payload.Speaker_user_id = current_speaker_id[tableid]
		if _, ok := current_speaker_id[tableid]; !ok {
			payload.Speaker_user_id = -1
		}
		l, _ := json.Marshal(payload)
		post := client.Publish("eugra/"+strconv.Itoa(tableid)+"/action", 0, false, l) //send message
		post.Wait()
	}
	if member.Action == "leave" {
		//check if user is inside the user array
		i := -1
		for k, v := range totalmembers[tableid] {
			if member.User_id == v {
				i = k
			}
		}
		//Remove user if user is in bufferqueue
		if _, ok := bufferqueue[tableid]; ok {
			bufferindex := -1
			for k, v := range bufferqueue[tableid] {
				if member.User_id == v {
					bufferindex = k
				}
			}
			if bufferindex != -1 && bufferindex < len(bufferqueue) {
				bufferqueue[tableid][bufferindex] = bufferqueue[tableid][len(bufferqueue)-1]
				bufferqueue[tableid][len(bufferqueue[tableid])-1] = 0
				bufferqueue[tableid] = bufferqueue[tableid][:len(bufferqueue[tableid])-1]
			}
			//need to disable 5 second rule if table is empty
			if len(bufferqueue[tableid]) == 0 {
				bufferbool[tableid] = false
			}
		}
		//Remove user if user is in queue
		if _, ok := queuemap[tableid]; ok {
			queueindex := -1
			for k, v := range queuemap[tableid] {
				if member.User_id == v {
					queueindex = k
				}
			}
			if queueindex != -1 && queueindex < len(queuemap) {
				queuemap[tableid][queueindex] = queuemap[tableid][len(queuemap)-1]
				queuemap[tableid][len(queuemap[tableid])-1] = 0
				queuemap[tableid] = queuemap[tableid][:len(queuemap[tableid])-1]
			}
		}
		//if nextspeaker leaves need to choose next speaker
		if nextspeaker_id[tableid] == member.User_id {
			delete(nextspeaker_id, tableid)
			ChooseNextSpeaker(tableid, client)
		}
		//if current speaker leaves
		if current_speaker_id[tableid] == member.User_id {
			var turn Turn
			turn.Action = "turn_end"
			turn.User_id = member.User_id
			endturn(turn, tableid, client)
		}
		//if user is not in table array
		if i < 0 || i > len(totalmembers[tableid])-1 {
			log.Println("MemberChange() out of range")
			return
		}
		//remove user from array
		totalmembers[tableid][i] = totalmembers[tableid][len(totalmembers[tableid])-1] // Copy last element to index i.
		totalmembers[tableid][len(totalmembers[tableid])-1] = 0                        // Erase last element (write zero value).
		totalmembers[tableid] = totalmembers[tableid][:len(totalmembers[tableid])-1]   // Truncate slice
		//if the table is empty add current time to lastmembertime.
		if len(totalmembers[tableid]) == 0 {
			lastmembertime[tableid] = time.Now()
		}
	}

}

//Function for Handling turns
func actionTurn(turn Turn, tableid int, client MQTT.Client) {
	//If action is turn_start setup hash map for audio
	if turn.Action == "turn_start" {
		//add current speaker
		current_speaker_id[tableid] = turn.User_id
		turnstarttime[tableid] = time.Now()
		log.Print("actionTurn() User_id: " + strconv.FormatInt(turn.User_id, 10) + " " + turn.Action + " on the table: " + strconv.Itoa(tableid))
		bufferhashmap[tableid] = []byte{}
		//remove nextspeaker_id
		if nextspeaker_id[tableid] == current_speaker_id[tableid] {
			if _, ok := nextspeaker_id[tableid]; ok {
				delete(nextspeaker_id, tableid)
			}
		}

	}
	//If action is turn_end Save Ogg file and add entry to DB
	if turn.Action == "turn_end" {
		endturn(turn, tableid, client)
	}
}

//Function of saving audio
func endturn(turn Turn, tableid int, client MQTT.Client) {
	//remove current speaker
	if _, ok := current_speaker_id[tableid]; ok {
		delete(current_speaker_id, tableid)
	}
	log.Print("endturn() User_id: " + strconv.FormatInt(turn.User_id, 10) + " " + turn.Action + " on the table: " + strconv.Itoa(tableid))
	turn_index := turn_indexmap[tableid]
	audio_length := ExportOpustoWav(tableid, turn_index)
	//Add turn entry to database
	query := `INSERT INTO eugra_turns (id, table_id, user_id, turn_index, created_date, audio_location, audio_length, audio_codec, audio_container, md5,scripts) VALUES (null,?,?,?,?,?,?,?,?,?,"")`
	res, err := SQL.Exec(query, tableid, turn.User_id, turn_indexmap[tableid], time.Now(), "audio/"+strconv.Itoa(tableid)+"/"+strconv.Itoa(turn_index)+".ogg", string(audio_length), "ogg", "ogg", "")
	if err != nil {
		log.Fatal("endturn() Cannot run insert statement", err)
	}
	id, _ := res.LastInsertId() //get turnid from db
	turn_indexmap[tableid]++    //increment turn index
	_, ok := turn_idmap[tableid]
	if !ok {
		turn_idmap[tableid] = map[int]int64{} //check of hash map for turn_id is setup if not it creates entry for map
	}
	turn_idmap[tableid][turn_index] = id //insert turnid for the current turn using turnindex (if theres is an incorrect turn index)
	//log.Println(turn_idmap[tableid][turn_index])
	//Insert Nods and Shake into database
	for _, gesture := range gesturesmap[tableid] {
		actionGesture(gesture, tableid)
	}
	//Clear gesturesmap when turn ends
	if _, ok := gesturesmap[tableid]; ok {
		delete(gesturesmap, tableid)
	}

	//Create MQTT message for the info about recorded audio
	var audioinfo Audio
	row := SQL.QueryRow("SELECT SUM(case when type = 0 then 1 else 0 end) as nod ,SUM(case when type = 1 then 1 else 0 end) as shake FROM `eugra_gestures` WHERE turn_id=?", id)
	var nods, shakes sql.NullInt64
	row.Scan(&nods, &shakes)
	audioinfo.Nod = nods.Int64
	audioinfo.Shake = shakes.Int64
	audioinfo.Action = "recorded"
	audioinfo.User_id = turn.User_id
	audioinfo.Turn_id = id
	audioinfo.Audio_location = "audio/" + strconv.Itoa(tableid) + "/" + strconv.Itoa(turn_index) + ".ogg"
	audioinfo.Audio_length = audio_length
	audioinfo.Audio_codec = "ogg"
	audioinfo.Audio_container = "ogg"
	i, _ := json.Marshal(audioinfo)
	token := client.Publish("eugra/"+strconv.Itoa(tableid)+"/action", 0, false, i) //publish message to action channel
	token.Wait()
	//check if no speaker is choosen and timer is running, cancel timer and choose next speaker
	if _, ok := nextspeaker_id[tableid]; !ok {
		if _, ok = timertime[tableid]; ok {
			quittimer[tableid] <- true
			delete(quittimer, tableid)
			go ChooseBufferSpeaker(tableid, client)
			return
		}
	}
	//if the queue for apply is not empty, run function to find next user
	if len(queuemap[tableid]) != 0 {
		ChooseNextSpeaker(tableid, client)
	}
	SpeechToText(id, tableid, audioinfo.Audio_location)

	// log: tableID, current speaker ID, next speaker id, current waiting queue, timer started?,
	currentSpeakerID := current_speaker_id[tableid]
	currentTimerCount, ok2 := timertime[tableid]
	nextSpeakerID := nextspeaker_id[tableid]
	logstr := fmt.Sprintf("tableID:%d, current speakerID:%d, next speakerID:%d, queue:%v, timer started:%t, timer:%d\n", tableid, currentSpeakerID, nextSpeakerID, queuemap, ok2, currentTimerCount)
	log.Println("end_turn() end, " + logstr)

}

//SpeechToText Function to Start GCS
func SpeechToText(turnid int64, tableid int, location string) error {
	response, err := http.Get("http://localhost:3006/speechtotext?turnid=" + strconv.FormatInt(turnid, 10) + "&tableid=" + strconv.Itoa(tableid) + "&uri=gs://eugra-audio/" + location)
	if err != nil {
		log.Printf("SpeechToText() GET SpeechToText failed %s\n", err)
	} else {
		data, _ := ioutil.ReadAll(response.Body)
		log.Println("SpeechToText() GET SpeechToText response: " + string(data))
	}
	return err
}

//Function for Handling for gestures, mainly creates entry in database for each nod and shakes
func actionGesture(gesture Gesture, table_id int) {
	log.Print("User_id: " + strconv.FormatInt(gesture.User_id, 10) + " " + gesture.Action + " on the table: " + strconv.Itoa(table_id))
	var gestureint int
	if gesture.Action == "nod" {
		gestureint = 0
	}
	if gesture.Action == "shake" {
		gestureint = 1
	}
	//Insert gesture into database
	query := `INSERT INTO eugra_gestures (id, type, user_id, turn_id, created_date, timestamp) VALUES (null,?,?,?,?,?)`
	_, err := SQL.Exec(query, gestureint, gesture.User_id, turn_idmap[table_id][turn_indexmap[table_id]-1], time.Now(), int64(time.Now().Sub(turnstarttime[table_id])/time.Millisecond))
	if err != nil {
		log.Fatal("actionGesture() Cannot run insert statement", err)
	}
}
