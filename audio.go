package main

import (
	"database/sql"
	"log"
	"regexp"
	"strconv"
	"time"

	MQTT "github.com/eclipse/paho.mqtt.golang"
)

//Hash Tables or Maps for Audio and Speaker data.
var bufferhashmap = map[int][]byte{} //buffer for audio for tableid
var bufferopusmap = map[int][][]byte{}
var turn_indexmap = map[int]int{}        //current turn index for tableid
var turn_idmap = map[int]map[int]int64{} //Map for turnid per. Stores each turn id in turn index.
var current_speaker_id = map[int]int64{} //Stores id of current speaker
var turnstarttime = map[int]time.Time{}

//Hash Tables or Maps for Choosing next speaker
var nextspeaker_id = map[int]int64{} //chosen next speaker
var bufferqueue = map[int][]int64{}  //buffer queue for the 5 sec when a user apply when queue is empty
var bufferbool = map[int]bool{}      //buffer bool to check if 5 sec is in progress.
var queuemap = map[int][]int64{}     //stores all id of the users in the queue for next speaker

//Hash Tables or Maps for members in the table
var lastmembertime = map[int]time.Time{} //stores time when the table was last empty
var totalmembers = map[int][]int64{}     //stores all the users in an array of the current table

//Gestures
var gesturesmap = map[int][]Gesture{} //stores gestures

//MQTT audio Handler
var audios MQTT.MessageHandler = func(client MQTT.Client, msg MQTT.Message) {
	//Get current topic from MQTT topic by extracting numbers from the topic address e.g eugra/0/audio
	topic := msg.Topic()
	reg, err := regexp.Compile("[^0-9]+")
	if err != nil {
		log.Fatal(err)
	}
	str1 := reg.ReplaceAllString(topic, "")
	tableid, _ := strconv.Atoi(str1)
	AddBlock(msg.Payload(), tableid) //Calls function to Adds the audio 640 bytes to audio maps
}

//JoinNewTable Function to setup environment variables for a new table.
func JoinNewTable(table_id string, client MQTT.Client) {
	//make turn_index = 0 & make map for totalmembers to empty array
	index, _ := strconv.Atoi(table_id)
	turn_indexmap[index] = 0
	totalmembers[index] = []int64{}

	//Subscribe to MQTT channels
	topicaudio := "eugra/" + table_id + "/audio"
	if Maudio := client.Subscribe(topicaudio, 0, audios); Maudio.Wait() && Maudio.Error() != nil {
		log.Panic(Maudio.Error())
	}
	topicaction := "eugra/" + table_id + "/action"
	if Maction := client.Subscribe(topicaction, 0, actions); Maction.Wait() && Maction.Error() != nil {
		log.Panic(Maction.Error())
	}
	ctrlupaction := "eugra/" + table_id + "/ctrl_up"
	if Mctrl := client.Subscribe(ctrlupaction, 0, ctrlup); Mctrl.Wait() && Mctrl.Error() != nil {
		log.Panic(Mctrl.Error())
	}
}

//Function to handle 640 bytes to the system.
func AddBlock(block []byte, topic int) {
	/*
		if len(block) == 640 { //check if bytes are 640 so no corrupted messages is inserted into audio
			for _, value := range block {
				bufferhashmap[topic] = append(bufferhashmap[topic], value) //append bytes to audio map
			}
		}*/

	bufferopusmap[topic] = append(bufferopusmap[topic], block)
	/*for _, value := range pcm {

		buf := new(bytes.Buffer)
		binary.Write(buf, binary.LittleEndian, value)
		for _, values := range buf.Bytes() {
			bufferhashmap[topic] = append(bufferhashmap[topic], values) //append bytes to audio map
		}
	}*/
}

//LeaveTable Function to remove environment variables to free up resources. unsub to mqtt, clear hash.
func LeaveTable(tableIDStr string, client MQTT.Client) {
	tableID, _ := strconv.Atoi(tableIDStr) //change tableid from string to int
	//remove tableid from the maps. require to check if the value exist before deleting
	if _, ok := bufferhashmap[tableID]; ok {
		delete(bufferhashmap, tableID)
	}
	if _, ok := turn_indexmap[tableID]; ok {
		delete(turn_indexmap, tableID)
	}
	if _, ok := turn_idmap[tableID]; ok {
		delete(turn_idmap, tableID)
	}
	if _, ok := queuemap[tableID]; ok {
		delete(queuemap, tableID)
	}
	if _, ok := nextspeaker_id[tableID]; ok {
		delete(nextspeaker_id, tableID)
	}
	if _, ok := bufferqueue[tableID]; ok {
		delete(bufferqueue, tableID)
	}
	if _, ok := bufferbool[tableID]; ok {
		delete(bufferbool, tableID)
	}
	if _, ok := totalmembers[tableID]; ok {
		delete(totalmembers, tableID)
	}
	if _, ok := current_speaker_id[tableID]; ok {
		delete(current_speaker_id, tableID)
	}
	if _, ok := lastmembertime[tableID]; ok {
		delete(lastmembertime, tableID)
	}
	if _, ok := timertime[tableID]; ok {
		delete(timertime, tableID)
	}
	if _, ok := quittimer[tableID]; ok {
		delete(quittimer, tableID)
	}

	//Unsubscribe to mqtt channels for the table
	topicaudio := "eugra/" + tableIDStr + "/audio"
	if Maudio := client.Unsubscribe(topicaudio); Maudio.Wait() && Maudio.Error() != nil {
		log.Panic(Maudio.Error())
	}
	topicaction := "eugra/" + tableIDStr + "/action"
	if Maction := client.Unsubscribe(topicaction); Maction.Wait() && Maction.Error() != nil {
		log.Panic(Maction.Error())
	}
	ctrlupaction := "eugra/" + tableIDStr + "/ctrl_up"
	if Mctrl := client.Unsubscribe(ctrlupaction); Mctrl.Wait() && Mctrl.Error() != nil {
		log.Panic(Mctrl.Error())
	}
	// need to check if there's any turns. if not, update the table states to disabled.
	var turnNum int = 0
	err := SQL.QueryRow("SELECT COUNT(id) FROM `eugra_turns` WHERE `table_id`=?", tableIDStr).Scan(&turnNum)
	if err != nil {
		if err == sql.ErrNoRows {
			// there were no rows, but otherwise no error occurred. no turns for this table. delete.
			SetTableDisable(int64(tableID))
			log.Printf("LeaveTable() leave table:%s, set disabled\n", tableIDStr)
		} else {
			log.Panicf("LeaveTable() error find turns for table:%s, err:%v\n", tableIDStr, err)
			SetTableExpire(int64(tableID))
			log.Printf("LeaveTable() leave table:%s, set expired\n", tableIDStr)
		}
	} else {
		SetTableExpire(int64(tableID))
		log.Printf("LeaveTable() leave table:%s, set expired\n", tableIDStr)
	}
}
