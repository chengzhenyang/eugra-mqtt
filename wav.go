package main

import (
	"bytes"
	"encoding/binary"
	"io"
	"log"
	"os"
	"strconv"
	"time"

	"eugra-mqtt/oggwriter"

	"github.com/go-audio/audio"
	"github.com/go-audio/wav"
)

func ExportOpustoWav(table_id int, turn_index int) string {
	//Use OpusWriter
	time := strconv.Itoa(len(bufferopusmap[table_id]) * 20)
	//Create Folder "audio" if it doesn't exist
	if _, err := os.Stat("audio"); os.IsNotExist(err) {
		os.Mkdir("audio", os.ModeDir)
		os.Chmod("audio", 0777) //fix permission on unix systems
	}
	//Create Foldedr "audio/0" if it doesn't exist
	if _, err := os.Stat("audio/" + strconv.Itoa(table_id)); os.IsNotExist(err) {
		os.Mkdir("audio/"+strconv.Itoa(table_id), os.ModeDir)
		os.Chmod("audio/"+strconv.Itoa(table_id), 0777) //fix permission on unix systems
	}
	opus, err := oggwriter.New("audio/"+strconv.Itoa(table_id)+"/"+strconv.Itoa(turn_index)+".ogg", 16000, 1)
	var timestamp uint64
	timestamp = 0
	for _, value := range bufferopusmap[table_id] {
		if err != nil {
			log.Println(err)
		}
		opus.WriteByte(value, timestamp)
		timestamp = timestamp + 960 //granule incrementation is suppose to be 1000 not 1.
	}
	opus.Close()
	log.Println(err)
	bufferopusmap[table_id] = [][]byte{}
	//upload audio file to google storage
	t := "audio/" + strconv.Itoa(table_id) + "/" + strconv.Itoa(turn_index) + ".ogg"

	err = write(gsclient, gsbucket, t, t)
	if err != nil {
		log.Println(err)
	} else {
		//del file
		os.Remove(t)
	}
	return time
}

//Function for saving audio into wav file
func ExportWav(table_id int, turn_index int) string {
	bufferbytes, ok := bufferhashmap[table_id]
	if !ok {
		log.Print("No bytes exist")
		return "failed"
	}
	//Create Folder "audio" if it doesn't exist
	if _, err := os.Stat("audio"); os.IsNotExist(err) {
		os.Mkdir("audio", os.ModeDir)
		os.Chmod("audio", 0777) //fix permission on unix systems
	}
	//Create Foldedr "audio/0" if it doesn't exist
	if _, err := os.Stat("audio/" + strconv.Itoa(table_id)); os.IsNotExist(err) {
		os.Mkdir("audio/"+strconv.Itoa(table_id), os.ModeDir)
		os.Chmod("audio/"+strconv.Itoa(table_id), 0777) //fix permission on unix systems
	}
	in := bytes.NewReader(bufferbytes)
	//Create File .wav in the right folder
	out, err := os.Create("audio/" + strconv.Itoa(table_id) + "/" + strconv.Itoa(turn_index) + ".wav")
	if err != nil {
		log.Fatal(err)
	}
	defer out.Close() //Close at the end.
	//Use audio module to convert pcm16 to wav.
	e := wav.NewEncoder(out, 16000, 16, 1, 1)
	audioBuf, err := newAudioIntBuffer(in)
	if err != nil {
		log.Fatal(err)
	}
	if err := e.Write(audioBuf); err != nil {
		log.Fatal(err)
	}
	if err := e.Close(); err != nil {
		log.Fatal(err)
	}
	bufferbytes = []byte{}
	//Save data from audio module back to created file
	outputfile, _ := os.Open("audio/" + strconv.Itoa(table_id) + "/" + strconv.Itoa(turn_index) + ".wav")
	length, _ := wav.NewDecoder(outputfile).Duration()
	outputfile.Close()
	log.Println("Saved Audio with the length of ", int64(length/time.Millisecond))
	//return the length of audio
	return strconv.FormatInt(int64(length/time.Millisecond), 10)
}

//Function for Saving Accessing Audio Module to convert pcm16 to wav
func newAudioIntBuffer(r io.Reader) (*audio.IntBuffer, error) {
	buf := audio.IntBuffer{
		Format: &audio.Format{
			NumChannels: 1,
			SampleRate:  16000,
		},
	}
	for {
		var sample int16
		err := binary.Read(r, binary.LittleEndian, &sample)
		switch {
		case err == io.EOF:
			return &buf, nil
		case err != nil:
			return nil, err
		}
		buf.Data = append(buf.Data, int(sample))
	}
}

//Function for Saving Accessing Audio Module to convert pcm16 to wav
func newAudioIntBufferOPUS(source [][]int16) *audio.IntBuffer {
	buf := audio.IntBuffer{
		Format: &audio.Format{
			NumChannels: 1,
			SampleRate:  16000,
		},
	}
	for _, value := range source {
		for _, data := range value {
			buf.Data = append(buf.Data, int(data))
		}
	}
	return &buf
}
