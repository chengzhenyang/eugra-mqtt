package main

import (
	"encoding/json"
	"fmt"
	"html"
	"log"
	"mime"
	"net/http"
	"path/filepath"
	"strconv"
)

//Handlers for Checking API
func IndexHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello, %q", html.EscapeString(r.URL.Path))
}

//Handler for creating a new table
func CreateTableHandler(w http.ResponseWriter, r *http.Request) {
	log.Print("./CreateTable ")
	id, ok := r.URL.Query()["id"]
	if !ok || len(id[0]) < 1 {
		response := Status{
			Status:  "400",
			Message: "Url Param 'id' is missing",
		}
		json.NewEncoder(w).Encode(response)
		return
	}
	JoinNewTable(id[0], client)
	response := Status{
		Status:  "200",
		Message: "created a new table with the table_id: " + id[0],
	}
	json.NewEncoder(w).Encode(response)
	log.Printf("CreateTableHandler() A new table has been created with the table_id:%s\n" + id[0])
}
func GetTableInfoHandler(w http.ResponseWriter, r *http.Request) {
	log.Print("./Info ")
	id, ok := r.URL.Query()["id"]
	if !ok || len(id[0]) < 1 {
		tables := []int{}
		for k := range turn_indexmap {
			tables = append(tables, k)
		}
		response := Info{
			Tables: tables,
		}
		json.NewEncoder(w).Encode(response)
		return
	}
	tableid, _ := strconv.Atoi(id[0])
	response := InfoTable{
		Current_speaker_id: current_speaker_id[tableid],
		TotalMember:        totalmembers[tableid],
		Turn_index:         turn_indexmap[tableid],
		Queuemap:           queuemap[tableid],
		Lastmembertime:     lastmembertime[tableid],
	}
	json.NewEncoder(w).Encode(response)
}

//Handler for removing an old table
func RemoveTableHandler(w http.ResponseWriter, r *http.Request) {
	log.Print("./RemoveTable ")
	id, ok := r.URL.Query()["id"]
	if !ok || len(id[0]) < 1 {
		response := Status{
			Status:  "400",
			Message: "Url Param 'id' is missing",
		}
		json.NewEncoder(w).Encode(response)
		return
	}
	LeaveTable(id[0], client)
	response := Status{
		Status:  "200",
		Message: "the Table has been Removed with the table_id: " + id[0],
	}
	json.NewEncoder(w).Encode(response)
	log.Print("the Table has been Removed with the table_id: " + id[0])
}

//Handler for handling static wav files
func FileServer(w http.ResponseWriter, r *http.Request) {
	//get the file path the user wants to access
	filename := "." + r.URL.Path

	w.Header().Set("Content-Type", mime.TypeByExtension(filepath.Ext(filename)))
	http.ServeFile(w, r, filename)
	log.Print("File Audio Get: " + filename)
}

//Speech struct to send to mqtt
type Speech struct {
	TurnID int64  `json:"turn_id"`
	Ctrl   string `json:"ctrl"`
	Script string `json:"script"`
}

//speech struct to send to eugra-mqtt
type speechResult struct {
	TurnID  int64  `json:"turn_id"`
	TableID int64  `json:"table_id"`
	Script  string `json:"script"`
}

//SpeechToTextResultHandler receive script result, send to mqtt
func SpeechToTextResultHandler(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var result speechResult
	err := decoder.Decode(&result)
	if err != nil {
		log.Panicf("SpeechToTextResultHandler() error decode request:%v, err:%v\n", r.Body, err)
	}
	/*
		turnid, ok := r.URL.Query()["turnid"]
		if !ok || len(turnid[0]) < 1 {
			response := Status{
				Status:  "400",
				Message: "Url Param 'turnid' is missing",
			}
			json.NewEncoder(w).Encode(response)
			log.Println("SendSpeechToTexts error: turnid missing")
			return
		}
		script, ok := r.URL.Query()["script"]
		if !ok || len(script[0]) < 1 {
			response := Status{
				Status:  "400",
				Message: "Url Param 'script' is missing",
			}
			json.NewEncoder(w).Encode(response)
			log.Println("SendSpeechToTexts error: script missing")
			return
		}
		tableid, ok := r.URL.Query()["tableid"]
		if !ok || len(script[0]) < 1 {
			response := Status{
				Status:  "400",
				Message: "Url Param 'tableid' is missing",
			}
			json.NewEncoder(w).Encode(response)
			log.Println("SendSpeechToTexts error: tableid missing")
			return
		}
		turnID, _ := strconv.ParseInt(turnid[0], 10, 64)
	*/
	var data Speech
	data.TurnID = result.TurnID
	data.Script = result.Script
	data.Ctrl = "new_script"

	i, _ := json.Marshal(data)
	token := client.Publish(fmt.Sprintf("eugra/%d/ctrl_down", result.TableID), 0, false, i) //publish message to action channel
	token.Wait()
	response := Status{
		Status:  "200",
		Message: "script published",
	}
	json.NewEncoder(w).Encode(response)

	//now need to check if this turn is the first in the table. if so, refresh home cache
	// if this turn is the very first turn of the table, need to notify api to refresh API.
	CheckRefreshHomeCache(result.TableID)

	log.Printf("SpeechToTextResultHandler() script published for turn:%d\n", result.TurnID)
}
