package main

import "time"

//Status model for REST API response
type Status struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}

type Info struct {
	Tables []int `json:"tables"`
}

type InfoTable struct {
	Current_speaker_id int64     `json:"current_speaker_id"`
	TotalMember        []int64   `json:"totalmembers"`
	Turn_index         int       `json:"turn_index"`
	Queuemap           []int64   `json:"queuemap"`
	Lastmembertime     time.Time `json:"lastmembertime"`
}

//Turn model for MQTT messages in Action
type Turn struct {
	User_id int64  `json:"user_id"`
	Action  string `json:"action"`
}

//Gesture model for MQTT messages in Action
type Gesture struct {
	User_id int64  `json:"user_id"`
	Action  string `json:"action"`
}

//Audio model for MQTT messages in Audio
type Audio struct {
	Action          string `json:"action"`
	User_id         int64  `json:"user_id"`
	Turn_id         int64  `json:"turn_id"`
	Audio_location  string `json:"audio_location"`
	Audio_length    string `json:"audio_length"`
	Audio_codec     string `json:"audio_codec"`
	Audio_container string `json:"audio_container"`
	Nod             int64  `json:"nod"`
	Shake           int64  `json:"shake"`
}

//CtrlUp model for MQTT messages in CtrlUp
type CtrlUp struct {
	User_id int64  `json:"user_id"`
	Ctrl    string `json:"ctrl"`
}

//CtrlDown model for MQTT messages in CtrlDown
type CtrlDown struct {
	User_id int64  `json:"user_id"`
	Ctrl    string `json:"ctrl"`
}

//Member model for MQTT message when a new member joins
type Member struct {
	User_id int64  `json:"user_id"`
	Action  string `json:"action"`
}

//TotalMember model for MQTT message for return to user the current speaker and turn info
type TotalMember struct {
	Action          string `json:"action"`
	Total_user      int    `json:"total_user"`
	Speaker_user_id int64  `json:"speaker_user_id"`
	Speaker_name    string `json:"speaker_name"`
}

//TextTurn model for MQTT message when a user fired a text content
type TextTurn struct {
	UserID int64  `json:"user_id"`
	Ctrl   string `json:"ctrl"`
	Name   string `json:"user_display_name"`
	Script string `json:"script"`
}

//TextTurnAck model for sending MQTT message to users, after processing the text turn
type TextTurnAck struct {
	TurnID int64  `json:"turn_id"`
	UserID int64  `json:"user_id"`
	Ctrl   string `json:"ctrl"`
	Name   string `json:"user_display_name"`
	Script string `json:"script"`
}

//TextTurnRej model for sending MQTT message to users, after failed to process the text turn
type TextTurnRej struct {
	UserID int64  `json:"user_id"`
	Ctrl   string `json:"ctrl"`
}

//ModifyText model for MQTT message when a user wants to modify a text content
type ModifyText struct {
	UserID int64  `json:"user_id"`
	TurnID int64  `json:"turn_id"`
	Ctrl   string `json:"ctrl"`
	Script string `json:"script"`
}

//ModifyTextAck model for sending MQTT message to users, after processing the modified text
type ModifyTextAck struct {
	UserID int64  `json:"user_id"`
	TurnID int64  `json:"turn_id"`
	Ctrl   string `json:"ctrl"`
	Script string `json:"script"`
}

//ModifyTextRej model for sending MQTT message to users, after processing the modified text
type ModifyTextRej struct {
	UserID int64  `json:"user_id"`
	TurnID int64  `json:"turn_id"`
	Ctrl   string `json:"ctrl"`
}
