package main

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"regexp"
	"strconv"
	"time"

	MQTT "github.com/eclipse/paho.mqtt.golang"
)

//MQTT handler for ctrlup messages
var ctrlup MQTT.MessageHandler = func(client MQTT.Client, msg MQTT.Message) {
	//Get current topic from MQTT topic by extracting numbers from the topic address e.g eugra/0/audio
	topic := msg.Topic()
	reg, err := regexp.Compile("[^0-9]+")
	if err != nil {
		log.Fatal(err)
	}
	str1 := reg.ReplaceAllString(topic, "")
	tableid, _ := strconv.Atoi(str1)
	//Unmarshal to Ctrl up and call ctrl_up function for apply
	var ctrl CtrlUp
	json.Unmarshal(msg.Payload(), &ctrl)
	if ctrl.Ctrl == "apply" {
		apply(ctrl, tableid, client)
	} else if ctrl.Ctrl == "text_turn" {
		var textTurn TextTurn
		json.Unmarshal(msg.Payload(), &textTurn)
		ctrlTextTurn(textTurn, tableid, client)
	} else if ctrl.Ctrl == "modify_text" {
		var modifyText ModifyText
		json.Unmarshal(msg.Payload(), &modifyText)
		ctrlModifyText(modifyText, tableid, client)
	}
}

//Function for registering apply
func apply(ctrl CtrlUp, table_id int, client MQTT.Client) {
	//if total_member is equal to 1 apply is now speaker

	// log: tableID, current speaker ID, next speaker id, current waiting queue, timer started?,
	currentSpeakerID, ok := current_speaker_id[table_id]
	currentTimerCount, ok2 := timertime[table_id]
	nextSpeakerID, ok3 := nextspeaker_id[table_id]
	logstr := fmt.Sprintf("tableID:%d, current speakerID:%d, next speakerID:%d, queue:%v, timer started:%t, timer:%d\n", table_id, currentSpeakerID, nextSpeakerID, queuemap, ok2, currentTimerCount)
	log.Println("apply() start, " + logstr)

	if len(totalmembers[table_id]) == 1 || (!ok && !ok2 && !bufferbool[table_id] && len(bufferqueue[table_id]) == 0 && len(queuemap[table_id]) == 0 && !ok3) {
		nextspeaker_id[table_id] = ctrl.User_id
		log.Println("apply() accepting cause empty table", ctrl.User_id)
		ApproveSpeaker(table_id, client)
		return
	}
	//if queue is empty apply 5 sec rule with buffer
	if len(queuemap[table_id]) == 0 && !ok3 {
		if bufferbool[table_id] {
			bufferqueue[table_id] = append(bufferqueue[table_id], ctrl.User_id)
		} else {
			bufferqueue[table_id] = append(bufferqueue[table_id], ctrl.User_id)
			if len(bufferqueue[table_id]) == 1 {
				log.Print("apply() start timer")
				if _, ok := quittimer[table_id]; !ok {
					quittimer[table_id] = make(chan bool)
				}
				go timer(ctrl, table_id, client)
				bufferbool[table_id] = true
			}
		}
		queuemap[table_id] = []int64{}
	} else {
		//if queue is not empty adds user to queue
		log.Print("apply() adding to queue")
		queuemap[table_id] = append(queuemap[table_id], ctrl.User_id)
	}

	// log: tableID, current speaker ID, next speaker id, current waiting queue, timer started?,
	currentSpeakerID, ok = current_speaker_id[table_id]
	currentTimerCount, ok2 = timertime[table_id]
	nextSpeakerID, ok3 = nextspeaker_id[table_id]
	logstr = fmt.Sprintf("tableID:%d, current speakerID:%d, next speakerID:%d, queue:%v, timer started:%t, timer:%d\n", table_id, currentSpeakerID, nextSpeakerID, queuemap, ok2, currentTimerCount)
	log.Println("apply() end, " + logstr)

}

func removeapply(ctrl CtrlUp, tableId int, client MQTT.Client) {

}

//Variables for 5 sec timer
var quittimer = make(map[int]chan bool)
var timertime = map[int]int{}

//Timer for 5 sec rule
func timer(ctrl CtrlUp, table_id int, client MQTT.Client) {
	bufferbool[table_id] = true
	//Listener for quit
	for {
		select {
		case quit := <-quittimer[table_id]:
			if quit {
				delete(timertime, table_id)
				bufferbool[table_id] = false
				log.Println("timer() closed", table_id)
				return
			}
		default:
			if _, ok := timertime[table_id]; !ok {
				if bufferbool[table_id] == true {
					timertime[table_id] = 5
				} else {
					return
				}
			} else {
				timertime[table_id] = timertime[table_id] - 1
				if timertime[table_id] == 0 {
					if _, ok = current_speaker_id[table_id]; ok {
						go ChooseBufferSpeaker(table_id, client)
					}
					bufferbool[table_id] = false
					delete(timertime, table_id)
				} else {
					time.Sleep(1 * time.Second)
				}
			}
			log.Println("timer() table ", table_id, " time ", timertime[table_id])
		}
	}
}

//ChooseNextSpeaker Function for choosing next speaker
func ChooseNextSpeaker(table_id int, client MQTT.Client) {
	rand.Seed(time.Now().Unix())
	if len(queuemap[table_id]) != 0 {
		i := rand.Intn(len(queuemap[table_id]))
		if _, ok := queuemap[table_id]; ok {
			nextspeaker_id[table_id] = queuemap[table_id][i]
		}
		queuemap[table_id][i] = queuemap[table_id][len(queuemap[table_id])-1] // Copy last element to index i.
		queuemap[table_id][len(queuemap[table_id])-1] = 0                     // Erase last element (write zero value).
		queuemap[table_id] = queuemap[table_id][:len(queuemap[table_id])-1]   // Truncate slice
	}

	ApproveSpeaker(table_id, client)

	// log: tableID, current speaker ID, next speaker id, current waiting queue, timer started?,
	currentSpeakerID := current_speaker_id[table_id]
	currentTimerCount, ok2 := timertime[table_id]
	nextSpeakerID := nextspeaker_id[table_id]
	logstr := fmt.Sprintf("tableID:%d, current speakerID:%d, next speakerID:%d, queue:%v, timer started:%t, timer:%d\n", table_id, currentSpeakerID, nextSpeakerID, queuemap, ok2, currentTimerCount)
	log.Println("ChooseNextSpeaker() end, " + logstr)

}

//ChooseBufferSpeaker Function for choosing next speaker for 5 sec rule
func ChooseBufferSpeaker(table_id int, client MQTT.Client) {
	rand.Seed(time.Now().Unix())
	log.Println("ChooseBufferSpeaker() bufferqueue:", bufferqueue[table_id])
	if len(bufferqueue[table_id]) != 0 {
		i := rand.Intn(len(bufferqueue[table_id]))
		if _, ok := bufferqueue[table_id]; ok {
			nextspeaker_id[table_id] = bufferqueue[table_id][i]
			bufferqueue[table_id][i] = bufferqueue[table_id][len(bufferqueue[table_id])-1] // Copy last element to index i.
			bufferqueue[table_id][len(bufferqueue[table_id])-1] = 0                        // Erase last element (write zero value).
			bufferqueue[table_id] = bufferqueue[table_id][:len(bufferqueue[table_id])-1]   // Truncate slice
			for _, element := range bufferqueue[table_id] {
				queuemap[table_id] = append(queuemap[table_id], element)
			}
		}
		bufferqueue[table_id] = []int64{}
	}
	log.Println("ChooseBufferSpeaker() nextspeaker_id:", nextspeaker_id[table_id])
	ApproveSpeaker(table_id, client)
}

//Send Approve message
func ApproveSpeaker(table_id int, client MQTT.Client) {
	var text CtrlDown
	text.User_id = nextspeaker_id[table_id]
	text.Ctrl = "apply_approved"
	i, _ := json.Marshal(text)
	//publish nextspeaker in MQTT
	nextspeaker := client.Publish("eugra/"+strconv.Itoa(table_id)+"/ctrl_down", 0, false, i)
	nextspeaker.Wait()
	log.Println("ApproveSpeaker() apply_approved", nextspeaker_id[table_id])
}

//ctrlTextTurn when a user fires a turn contains text only, should
// insert into database, but do not insert any audio related fields.
func ctrlTextTurn(textTurn TextTurn, tableid int, client MQTT.Client) {

	turnIndex := turn_indexmap[tableid]
	//Add turn entry to database
	query := `INSERT INTO eugra_turns (id, table_id, user_id, turn_index, created_date, audio_location, audio_length, audio_codec, audio_container, md5, scripts) VALUES (null,?,?,?,?,?,?,?,?,?,?)`
	res, err := SQL.Exec(query, tableid, textTurn.UserID, turnIndex, time.Now(), "", 0, "text", "text", "", textTurn.Script)
	if err != nil {
		log.Println("ctrlTextTurn() error run insert statement", err)
		var data TextTurnRej
		data.Ctrl = "text_turn_rej"
		data.UserID = textTurn.UserID
		jsonData, err2 := json.Marshal(data)
		if err2 != nil {
			log.Println("ctrlTextTurn() error parse jason", err2)
			return
		}
		token := client.Publish(fmt.Sprintf("eugra/%d/ctrl_down", tableid), 0, false, jsonData) //publish message to action channel
		token.Wait()
		return
	}
	turnID, _ := res.LastInsertId() //get turnid from db
	log.Printf("ctrlTextTurn() ctrl text_turn userID:%d, turnIndex:%d, turnID:%d, tableID:%d\n", textTurn.UserID, turnIndex, turnID, tableid)
	turn_indexmap[tableid]++ //increment turn index
	_, ok := turn_idmap[tableid]
	if !ok {
		turn_idmap[tableid] = map[int]int64{} //check of hash map for turn_id is setup if not it creates entry for map
	}
	turn_idmap[tableid][turnIndex] = turnID //insert turnid for the current turn using turnindex (if theres is an incorrect turn index)

	var data TextTurnAck
	data.TurnID = turnID
	data.Script = textTurn.Script
	data.Ctrl = "text_turn_ack"
	data.UserID = textTurn.UserID
	data.Name = textTurn.Name

	jsonData, _ := json.Marshal(data)
	token := client.Publish(fmt.Sprintf("eugra/%d/ctrl_down", tableid), 0, false, jsonData) //publish message to action channel
	token.Wait()

	CheckRefreshHomeCache(int64(tableid))
}

//ctrlModifyText when a user modify his script. no matter this turn contains audio or not.
// update database, but do not insert any audio related fields.
func ctrlModifyText(modifyText ModifyText, tableid int, client MQTT.Client) {

	//Add turn entry to database
	//query := "UPDATE `eugra_turns` SET `scripts` = '$1' WHERE `id` = $2"
	sqlStatement := "UPDATE `eugra_turns` SET `scripts` = ? WHERE `eugra_turns`.`id` = ?"
	_, err := SQL.Exec(sqlStatement, modifyText.Script, modifyText.TurnID)
	if err != nil {
		log.Println("ctrlModifyText() error run UPDATE statement", err)
		var data ModifyTextRej
		data.TurnID = modifyText.TurnID
		data.Ctrl = "modify_text_rej"
		data.UserID = modifyText.UserID
		jsonData, err2 := json.Marshal(data)
		if err2 != nil {
			log.Println("ctrlModifyText() error parse jason", err2)
			return
		}
		token := client.Publish(fmt.Sprintf("eugra/%d/ctrl_down", tableid), 0, false, jsonData) //publish message to action channel
		token.Wait()
		return
	}

	log.Printf("ctrlModifyText() ctrl modify_text userID:%d, turnID:%d, tableID:%d\n", modifyText.UserID, modifyText.TurnID, tableid)

	var data ModifyTextAck
	data.TurnID = modifyText.TurnID
	data.Script = modifyText.Script
	data.Ctrl = "modify_text_ack"
	data.UserID = modifyText.UserID

	jsonData, _ := json.Marshal(data)
	token := client.Publish(fmt.Sprintf("eugra/%d/ctrl_down", tableid), 0, false, jsonData) //publish message to action channel
	token.Wait()
}
