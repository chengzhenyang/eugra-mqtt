package main

import (
	"context"
	"flag"
	"fmt"
	"io"
	"log"
	"os"

	"cloud.google.com/go/storage"
)

func GoogleStorage() *storage.Client {
	//export GOOGLE_CLOUD_PROJECT="eugra-nz"
	//export GOOGLE_APPLICATION_CREDENTIALS="/Users/scorpionknifes/eugra.json"
	//os.Setenv("GOOGLE_CLOUD_PROJECT", "eugra-nz")
	//os.Setenv("GOOGLE_APPLICATION_CREDENTIALS", "C:/Users/cheng/go/src/eugra-mqtt/eugra.json")
	projectID := os.Getenv("GOOGLE_CLOUD_PROJECT")
	if projectID == "" {
		fmt.Fprintf(os.Stderr, "GOOGLE_CLOUD_PROJECT environment variable must be set.\n")
		os.Exit(1)
	}
	var o string
	flag.StringVar(&o, "o", "", "source object; in the format of <bucket:object>")
	flag.Parse()

	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	if err != nil {
		log.Fatal(err)
	}
	return client
}

func write(client *storage.Client, bucket, object string, file string) error {
	ctx := context.Background()
	// [START upload_file]
	f, err := os.Open(file)
	if err != nil {
		return err
	}
	defer f.Close()

	wc := client.Bucket(bucket).Object(object).NewWriter(ctx)
	if _, err = io.Copy(wc, f); err != nil {
		return err
	}
	if err := wc.Close(); err != nil {
		return err
	}
	// [END upload_file]
	return nil
}
