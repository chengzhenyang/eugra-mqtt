package main

import (
	"context"
	"database/sql"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"

	"cloud.google.com/go/storage"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"github.com/robfig/cron"

	MQTT "github.com/eclipse/paho.mqtt.golang"
)

const local string = "LOCAL"

//SQL & MQTT settings
var SQL *sql.DB
var client MQTT.Client
var gsclient *storage.Client
var gsbucket = "eugra-audio"

//Function to Start APP
func main() {
	gsclient = GoogleStorage()
	var (
		// environment variables
		env        = os.Getenv("ENV")  // LOCAL, DEV, STG, PRD
		port       = os.Getenv("PORT") // server traffic on this port
		connection = os.Getenv("SQL")
		mqtt       = os.Getenv("MQTT")
	)
	if env == "" || env == local {
		env = local
		port = "3002"
		connection = "eugra:eugra@tcp(114.23.220.120:3306)/eugra-api?parseTime=true"
		mqtt = "tcp://114.23.220.120:1883" //Connection Address
	}
	//Initialise Database Connection
	var errs error
	SQL, errs = sql.Open("mysql", connection)
	if errs != nil {
		log.Fatal(errs)
	}
	defer SQL.Close()

	//Initialise Mqtt Connection
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	//Mqtt connection settings
	opts := MQTT.NewClientOptions().AddBroker(mqtt)
	opts.SetClientID("eugra-MQTT")
	opts.OnConnect = func(c MQTT.Client) {}
	opts.CleanSession = false
	opts.Username = "eugra"
	opts.Password = "eugra-password"
	//Mqtt client name
	client = MQTT.NewClient(opts)
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		log.Println("Error connecting to mqtt")
		panic(token.Error())
	} else {
		log.Println("Connected to server")
	}
	//Start Crons for deleting tables with empty members
	crons := cron.New()
	crons.AddFunc("@hourly", func() { LeaveEmptyTables() })
	crons.Start()
	//Setup REST API for microservices
	CheckAllAtStartup()
	StartAPI(port)

}

//StartAPI Simple REST API
func StartAPI(port string) {
	router := mux.NewRouter()

	//Handlers for REST API
	router.HandleFunc("/", IndexHandler)
	router.HandleFunc("/createtable", CreateTableHandler)
	router.HandleFunc("/removetable", RemoveTableHandler)
	router.HandleFunc("/info", GetTableInfoHandler)
	router.HandleFunc("/speechtotext_result", SpeechToTextResultHandler)
	router.HandleFunc("/audio/{category}/{category}", FileServer)

	srv := &http.Server{
		Addr:    ":" + port, //Connection Port
		Handler: router,
	}
	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
	//Check REST API Connection
	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()
	log.Print("Server Started")

	//Function to Stop Server
	<-done
	log.Print("Server Stopped")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	client.Disconnect(0)
	defer func() {
		// extra handling here
		cancel()
	}()

	if err := srv.Shutdown(ctx); err != nil {
		log.Fatalf("Server Shutdown Failed:%+v", err)
	}
	log.Print("Server Exited Properly")
}

//LeaveEmptyTables Function to remove tables that are older than 1 day with zero users
func LeaveEmptyTables() {
	log.Print("LeaveEmptyTables() clearing all empty tables?")
	//https://dave.cheney.net/2017/04/29/there-is-no-pass-by-reference-in-go
	// because we are deleting map inside LeaveTable func, should save to a temp for the loop.
	tempLastActiveTime := lastmembertime
	for tableID, v := range tempLastActiveTime {
		if time.Now().Sub(v) > 24*time.Hour {
			log.Printf("LeaveEmptyTables() leave table %d\n", tableID)
			LeaveTable(strconv.Itoa(tableID), client)
		}
	}
	// states for table may have changed.
	RefreshHomeCache()
}

//CheckRefreshHomeCache check if a new valid turn was inserted
func CheckRefreshHomeCache(tableID int64) error {
	// if a turn is the very first turn of the table, need to notify api to refresh API.
	var numOfTurns int = 0
	row := SQL.QueryRow("SELECT COUNT(*) from `eugra_turns` WHERE `table_id`=? AND `scripts` IS NOT NULL AND `scripts` <> ''", tableID)
	err := row.Scan(&numOfTurns)
	if err != nil {
		log.Panicf("CheckRefreshHomeCache() error select count on table:%d, err:%v\n", tableID, err)
		return err
	}

	if numOfTurns == 1 {
		// only one turn has script, refresh.
		err = RefreshHomeCache()
		if err != nil {
			log.Panicf("CheckRefreshHomeCache() error refresh home cache, err:%v\n", err)
			return err
		}
	}
	return nil
}

//RefreshHomeCache notify api to refresh its cache
func RefreshHomeCache() error {
	response, err := http.Get("http://localhost:3001/refresh_home_cache")
	if err != nil {
		log.Panicf("RefreshHomeCache() GET /refresh_home_cache failed %s\n", err)
	} else {
		data, _ := ioutil.ReadAll(response.Body)
		log.Println("RefreshHomeCache() GET /refresh_home_cache response: " + string(data))
	}
	return err
}

//CheckAllAtStartup Function to check tables and sub to tables that are active. and only if within 24 hours, there's no new turn, that we could mark it as expired.
func CheckAllAtStartup() {
	log.Println("CheckAllAtStartup() checking all")
	rows, err := SQL.Query("SELECT `id`, `created_date` FROM `eugra_tables` WHERE `states`=2")
	if err != nil {
		log.Printf("CheckAllAtStartup() error performing sql, err:%v\t", err)
		return
	}
	defer rows.Close()
	for rows.Next() {
		var createdtime time.Time
		var tableid int64
		err = rows.Scan(&tableid, &createdtime)
		if err != nil {
			log.Printf("CheckAllAtStartup() error scan row, err:%v\t", err)
			continue
		}
		if time.Now().Sub(createdtime) > time.Hour*24 {
			// now need to check turns.
			row := SQL.QueryRow("SELECT `created_date` as date FROM `eugra_turns` WHERE `table_id`=? ORDER BY date DESC LIMIT 1", tableid)
			var turnDate time.Time
			err = row.Scan(&turnDate)
			if err == sql.ErrNoRows {
				// doesn't have any turns.
				log.Printf("CheckAllAtStartup() no turns for table:%d, disable now. err:%v\t", tableid, err)
				SetTableDisable(tableid)
				continue
			} else if err != nil {
				log.Printf("CheckAllAtStartup() error scan row, tableID:%d, err:%v\t", tableid, err)
				continue
			}
			// has turn, check time.
			if time.Now().Sub(turnDate) > time.Hour*24 {
				log.Printf("CheckAllAtStartup() table:%d has turns older than 24 hours. expire now\n", tableid)
				SetTableExpire(tableid)
			} else {
				log.Printf("CheckAllAtStartup() table:%d has turns within 24 hours. join now\n", tableid)
				JoinNewTable(strconv.FormatInt(tableid, 10), client)
			}
		} else {
			log.Printf("CheckAllAtStartup() table:%d was created within 24 hours. join now\n", tableid)
			JoinNewTable(strconv.FormatInt(tableid, 10), client)
		}
	}
}

//SetTableExpire set sql table state to 1 (expired)
func SetTableExpire(tableID int64) {
	_, err := SQL.Query("UPDATE `eugra_tables` SET `states` = '1' WHERE `eugra_tables`.`id` = ?", tableID)
	if err != nil {
		log.Panicf("SetTableExpire() error update table:%d, err:%v\n", tableID, err)
	}
}

//SetTableDisable set sql table state to 0 (disabled)
func SetTableDisable(tableID int64) {
	_, err := SQL.Query("UPDATE `eugra_tables` SET `states` = '0' WHERE `eugra_tables`.`id` = ?", tableID)
	if err != nil {
		log.Panicf("SetTableExpire() error update table:%d, err:%v\n", tableID, err)
	}
	// then need to check user's favorite table list. user shouldn't see it as a favorite table anymore.

	_, err = SQL.Exec("DELETE FROM `eugra_favorite_tables` WHERE `table_id`=?", tableID)
	if err != nil {
		log.Panicf("SetTableExpire() error deleting table:%d from favorites, err:%v\n", tableID, err)
	}

}
